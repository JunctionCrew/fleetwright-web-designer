import { createApp, reactive } from 'vue'
import { parse } from 'yaml'
import App from './App.vue'

import './assets/main.less'

const app = createApp(App)
const globals = reactive({})
app.provide('globals', globals)
app.mount('#app')

// can't use await due to vue jank
fetch("junctspace_dump.yaml").then(
    (value) => {
        value.text().then(
            (text) => {
                // i swear this is the best way to do this
                // global variable? why would you want that?
                let data_dump = parse(text)
                console.log("done", data_dump)
                globals.data_dump = data_dump
            }
        )
    }
)